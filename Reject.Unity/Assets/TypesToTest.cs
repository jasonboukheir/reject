﻿using System;

namespace Reject.TestTypes
{
    [Serializable]
    public class Base
    {
        public bool isBase = true;
    }

    [Serializable]
    public class Derived : Base
    {
        public bool isDerived = true;
    }
}
