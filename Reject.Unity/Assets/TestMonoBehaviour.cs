﻿using Reject;
using Reject.TestTypes;
using UnityEngine;

public class TestMonoBehaviour : MonoBehaviour
{
    [SerializeReference]
    [Inject]
    public Base publicToInject;

    [SerializeReference]
    [SerializeField]
    [Inject]
    private Base privateToInject;
}
