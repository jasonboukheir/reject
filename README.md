# Install

Add the package to your manifest.json file located under the Packages folder.

```
dependencies: {
    "com.jasonboukheir.reject": "https://gitlab.com/jasonboukheir/reject.git",
    ...
}
```